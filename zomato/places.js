var axios = require('axios');
var request = require('request');
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient

var config = require('./config');
var Places = require('../places/Place.js');
var Listings = require('../listings/Listing.js');

var axiosInstance = axios.create({
  headers: { 'user-key': config.apiKey }
})

router.get('/', function(req, res) {
    let coordinates = {
      lat: req.query.lat ? req.query.lat : 12.94994,
      long: req.query.long ? req.query.long : 77.555,
      radius: req.query.radius ? req.query.radius : 5000
    }
    console.log(coordinates.lat)
    console.log(coordinates.long)
    axiosInstance.get(config.baseURL + '/search', {
      params: {
        lat: coordinates.lat,
        lon: coordinates.long,
        radius: coordinates.radius,
        category: 3,
        sort: 'real_distance'
      }
    })
    .then(function(response) {
      var places = response.data.restaurants;
      const filteredPlaces = [];
      let m = 0;
      places.map((place, i) => {
        if(place.restaurant.thumb) {
          filteredPlaces[m] = place;
          m++;
        }
        // return filteredPlaces;
      });
      res.send(filteredPlaces);
    })
    .catch(function(err) {
      console.log(err)
      res.send('<h1>Error</h1>');
    })
})

router.get('/listings', function(req, res) {
  var url = 'mongodb://shiva-venkatesh:Erebus26@cluster0-shard-00-00-ykqkg.mongodb.net:27017,cluster0-shard-00-01-ykqkg.mongodb.net:27017,cluster0-shard-00-02-ykqkg.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';

  MongoClient.connect(url, function(err, db) {
    if(err) {
      res.sendStatus(500)
    } else {
      console.log("Connected successfully to server");
      var response = db.places.find({ coordinates : { $near : { $geometry : {
                type : "Point" ,
                coordinates : [req.query.lomg, req.query.lat] },
                $maxDistance : 10000 /* 10 kms */
          }
        }
      })
      res.send(response)
    }
  });
});

router.get('/collections/all', function(req, res) {
  var options = { method: 'GET',
    url: 'https://developers.zomato.com/api/v2.1/collections',
    qs: { lat: req.query.lat, lon: req.query.long },
    headers:
     { 'cache-control': 'no-cache',
       'user-key': config.apiKey,
       Accept: 'application/json' }
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(JSON.parse(body));
    res.send(JSON.parse(body).collections);
  });
});

router.get('/collections/:id', function(req, res) {
  var options = { method: 'GET',
    url: 'https://developers.zomato.com/api/v2.1/search',
    qs: { radius: '5000', collection_id: req.params.id, lat: req.query.lat, lon: req.query.long },
    headers:
     { 'Postman-Token': 'e8e40abd-e0b5-4802-a9fd-fb2b865b9e64',
       'cache-control': 'no-cache',
       'user-key': '5c7623e88fe86025b5555877170e3a89',
       Accept: 'application/json' } };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
    res.send(JSON.parse(body).restaurants);
  });
});

router.get('/:id', function (req, res) {
  axiosInstance.get(config.baseURL + 'restaurant?res_id=' + req.params.id)
    .then(function(response) {
      var place = response.data;
      console.log(response.data);
      res.send(place);
    })
    .catch(function(err) {
      console.log('Error logged' + err);
      res.sendStatus(404)
    })
});

module.exports = router;
