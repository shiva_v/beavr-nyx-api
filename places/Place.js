var mongoose = require('mongoose');

var LocationSchema = new mongoose.Schema({
  address: String,
  locality: String,
  city: String,
  city_id: Number,
  coordinates: [Number],
  country_id: Number,
  locality_verbose: String
});

var PinSchema = new mongoose.Schema({
  location: {
    type: [Number],
    index: '2d'
  }
})

var PlaceSchema = new mongoose.Schema({
  name: {type: String, index: {dropDups: true}},
  zomato_url: String,
  zomato_id: Number,
  thumbnail: String,
  location: LocationSchema,
  pin: PinSchema,
  cuisines: String,
  cost_for_two: Number,
  user_rating: {
    aggregate_rating: String,
    rating_text: String,
    rating_color: String,
    votes: String
  },
  photos_url: String,
  menu_url: String,
  featured_image: String,
  zomato_deeplink: String,
  price_range: Number,
  category: String
});

mongoose.model('Place', PlaceSchema);

module.exports = mongoose.model('Place');
