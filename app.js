require('newrelic');
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan')
var elasticsearch = require('elasticsearch');
var db = require('./db');

global.__root   = __dirname + '/';

app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(express.static(__dirname + '/views'));
app.use(morgan('combined'));
app.use(require('express-status-monitor')());
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.disable('etag').disable('x-powered-by')

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});

app.get('/dashboard', function (req, res) {
  res.render('dashboard');
});

var UserController = require(__root + 'user/UserController');
app.use('/api/v1/users', UserController);

var CheckinController = require(__root + 'checkins/CheckinController');
app.use('/api/v1/checkins', CheckinController);

var EventsController = require(__root + 'events/EventsController');
app.use('/api/v1/events', EventsController);

var DashboardController = require(__root + 'dashboard/dashboard');
app.use('/api/v1/dashboard', DashboardController);

var AuthController = require(__root + 'auth/AuthController');
app.use('/api/v1/auth', AuthController);

var PlacesController = require(__root + 'zomato/places');
app.use('/api/v1/places', PlacesController)

module.exports = app;
