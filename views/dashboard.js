$.get('/api/v1/dashboard', function(data) {
  var users = data.users;
  var table = new Tabulator("#table-data", {
   	height:205, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
   	data:users, //assign data to table
   	layout:"fitColumns", //fit columns to width of table (optional)
   	columns:[ //Define Table Columns
  	 	{title:"ID", field:"_id", width:150},
  	 	{title:"name", field:"name"},
  	 	{title:"email", field:"email"},
  	 	{title:"password", field:"password"},
   	],
   	rowClick:function(e, row){ //trigger an alert message when the row is clicked
   		alert("Row " + row.getData().id + " Clicked!!!!");
   	},
  });
})
