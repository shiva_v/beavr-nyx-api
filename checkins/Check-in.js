var mongoose = require('mongoose');

var CheckinSchema = new mongoose.Schema({
  epochDate: Number,
  userID: String,
  zomato_id: Number,
  location: {
    address: String,
    locality: String,
    city: String,
    city_id: Number,
    coordinates: [Number],
    country_id: Number,
    locality_verbose: String
  }
});

mongoose.model('Checkin', CheckinSchema);

module.exports = mongoose.model('Checkin');
