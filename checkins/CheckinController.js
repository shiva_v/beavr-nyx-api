var request = require('request');
var express = require('express');
var router = express.Router();

var config = require('../config');
var Utils = require('../utils.js');
var Checkins = require('./Check-in.js');
var VerifyToken = require('../auth/VerifyToken.js');

router.post('/add', VerifyToken, function(req, res) {
  console.log('Controller POST');
  console.log('User ID from request is: ' + req.userId);
  console.log(req.body.zomato_id);
  Checkins.create({
          epochDate : Date.now(),
          userID : req.userID,
          zomato_id : req.body.zomato_id,
          location: {
            address: req.body.location.address,
            locality: req.body.location.locality,
            city: req.body.location.city,
            city_id: req.body.location.city_id,
            coordinates: req.body.location.coordinates,
            country_id: req.body.location.country_id,
            locality_verbose: req.body.location.locality_verbose
          }
      },
      function (err, checkin) {
          if (err) return res.status(500).send("There was a problem adding the information to the database.");
          res.status(200).json(checkin);
      });
});

module.exports = router;
