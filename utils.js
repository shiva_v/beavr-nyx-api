module.exports = {
  checkValidity: function(obj, expectedParams) {
    var validity = true;
    validity = expectedParams.map(function(param, index) {
      if(!obj.hasOwnProperty(param) || !obj[param]) return false
    });
    return validity;
  },
  getUserIDFromToken: function(req, res, next) {
    console.log('Get auth token from token middleware invoked')
    console.log(req.headers);
    var authToken = req.get('Authorization');
    console.log(authToken + ' is the Auth token');
    next(req, res, authToken);
  }
}
