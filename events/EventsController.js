var express = require('express');
var request = require('request');
var router = express.Router();
var bodyParser = require('body-parser');
var cache = require('memory-cache');

var redis = require("redis"),
    client = redis.createClient();
    
client.on("error", function (err) {
    console.log("Error " + err);
});

var VerifyToken = require(__root + 'auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
  var coords = {
    lat: req.query.lat,
    long: req.query.long
  }
  var cached_events = '';
  client.get("events");
  console.log('*(*(**))')
  console.log('cached events', cached_events)
  console.log('*(*(**))')
  if(cached_events) {
    console.log('Serving from cache')
    res.send(cached_events);
  } else {
    console.log("making a request")
    request('http://api.insider.in/tag/list?tags=homescreen-bangalore&model=event', function (error, response, body) {
      if(!error) {
        console.log(Array.isArray(JSON.parse(body).data.list))
        var list_of_events = JSON.parse(body).data.list;
        var filtered_events = list_of_events.filter(event => !(event.name.toLowerCase().includes('web') || event.name.toLowerCase().includes('app')))
        client.set('events', JSON.stringify(filtered_events), 'EX', 86400);
        res.send(filtered_events.slice(0,25));
      } else {
        res.sendStatus(500);
      }
    });
  }
});

router.get('/:id', function(req, res) {
  request('http://api.insider.in/event/getEventDetailsForClient/' + req.params.id, function (error, response, body) {
    if(!error) {
      console.log('Status:', response.statusCode);
      console.log('Headers:', JSON.stringify(response.headers));
      console.log('Response:', JSON.parse(body).data);
      res.send(JSON.parse(body).data);
    } else {
      res.sendStatus(500);
    }
  });
});


module.exports = router;
